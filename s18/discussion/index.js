// document.write('Discussion s18');

/*
    function printInput(){
        let nickname = prompt('Enter your nickname: ');
        return "Hi " + nickname;
    }
    console.log(printInput());
*/

/*----------------------------------------------*/

function printName(name){
    return "My name is " + name;
}
console.log(printName('Francis'));

/*----------------------------------------------*/
//variables can be passed as an argument
/*
    let anotherName = 'Mitsuha';
    console.log(printName(anotherName));
*/

/*----------------------------------------------*/
/*
    function printInput(nickname){
        nickname = prompt('Enter your nickname ');
        return "Hi " + nickname;
    }
    console.log(printInput());
*/
/*
    You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function.

    "name" is called a parameter.

    A "parameter" acts as a named variable/container that exists only inside of a function
    It is used to store information that is provided to a function when it is called/invoked.

    the information/data provided directly into the function, is called an argument. Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.
*/

function checkDivisivilityBy8(num){
    let remainder = num % 8;
    console.log("The remainder of " + num + "divided by 8 is : " + remainder);

    let isDivisibleBy8 = remainder === 0;
    console.log("Is " + num + " divisible by 8?");
    console.log(isDivisibleBy8);
}
checkDivisivilityBy8(64);
checkDivisivilityBy8(28);

/*
Function parameters can also accept other functions as arguments
Some complex functions use other functions as arguments to perform more complicated results.
This will be further seen when we discuss array methods.
*/

function argumentFunction(){
    console.log("This function was passed as an argument before the message was printed");
}

function invokeFunction(argumentFunction){
    argumentFunction();
}

invokeFunction(argumentFunction);
console.log(argumentFunction);

/*----------------------------------------------*/

/*
  Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.  
*/

function createFullName(firstName, middleName, lastName){
    return firstName + " " + middleName + " " + lastName;
}
console.log(createFullName("Juan", "Dela", "Cruz"));

