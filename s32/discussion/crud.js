let http = require("http");
const port = 4000;
let directory = [
    {
        "name": "Brandon",
        "email": "brandon@gmail.com",
    },
    {
        "name": "Brandy",
        "email": "brandy@gmail.com",  
    }
];

const app = http.createServer((req, res) => {
    if (req.url == "/users" && req.method == "GET"){
         // Sets response output to JSON data type
        res.writeHead(200, {"Content-type": "application/json"});
        // Input HAS to be data type STRING hence the JSON.stringify() method
        // This string input will be converted to desired output data type which has been set to JSON
        // This is done because requests and responses sent between client and a node JS server requires the inform
        res.write(JSON.stringify(directory));

        res.end();
    }

    if (req.url == "/users" && req.method == "POST"){
        // Declare and intialize a "requestBody" variable to an empty string
        // This will act as a placeholder for the resource/data to be created later on
        let requestBody = "";
        
        // A stream is a sequence of data
        // Data is received from the client and is processed in the "data" stream
        // The information provided from the request object enters a sequence called "data" the code below will be triggered
        // data step - this reads the "data" stream and processes it as the request body
        // Assigns the data retrieved from the data stream to requestBody
        req.on("data", (data) => {
            requestBody += data;
        })

        req.on("end", () => {
            console.log(typeof requestBody);

            //Convert JSON to javascript object
            requestBody = JSON.parse(requestBody);

            let newUser ={
                "name" : requestBody.name,
                "email" : requestBody.email
            }

            //add the new user to the mock database
            directory.push(newUser);   
            console.log(directory);

            res.writeHead(200, {"Content-Type": "application/json"});
            res.write(JSON.stringify(newUser));
       })

    }

    });
    
    app.listen(port, () => console.log(`Server running at localhost:${port}`));
    