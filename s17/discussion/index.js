//[SECTION] Functions
    // Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
    // Functions are mostly created to create complicated tasks to run several lines of code in succession
    // They are also used to prevent repeating lines/blocks of codes that perform the same task/function

    //(function statement) defines a function with the specified parameters.

    /*
        function functionName(){
        };
    */
    // function keyword - used to defined a javascript functions
    // functionName - the function name. Functions are named to be able to use later in the code.
    // function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.

        function printName(){    //declaration
            console.log("My name is Francis")
        }
        printName();  //invocation or calling the function

    //The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
        //It is common to use the term "call a function" instead of "invoke a function".

    // printAge(); - Error not declared. Uncaught ReferenceError: printAge is not defined

//[SECTION ] Function declarations vs Expressions

    //A function can be created through function declaration by using the function keyword and adding a function name.

    //Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).

    //function declared after invocation
    declaredFunction();

    function declaredFunction(){ 
        console.log('Hello form the other side');
    }

    declaredFunction();

    //Note: Hoisting is Javascript's behavior for certain variables and functions to run or use them before their declaration.

//Function expression
//A function can also be stored in a variable. This is called a function expression.
//A function expression is an anonymous function assigned to the variableFunction
// anonymous function - a function without a name

let variableFunction = function (){
    console.log("I am Ironman")  
};
variableFunction(); // cannot be hoisted. Must be called after the declaration

let funcExpression = function funcName(){
    console.log("I am Batman")    
}
funcExpression(); 

//A function expression of a function named func_name assigned to the variable funcExpression
//How do we invoke the function expression?
//They are always invoked (called) using the variable name.

//You can reassign declared functions and function expressions to new anonymous functions.

declaredFunction = function(){
    console.log("Updated declaredFunction")    
}
declaredFunction();

funcExpression = function(){
    console.log("Updated funcExpression")    
}
funcExpression();

const constantFunc = function(){  //used const. working but cannot be reassigned
    console.log("Initialized with const")    
}

//[SECTION] Function Scoping
/*	
Scope is the accessibility (visibility) of variables.

Javascript Variables has 3 types of scope:
    1. local/block scope
    2. global scope
    3. function scope
        JavaScript has function scope: Each function creates a new scope.
        Variables defined inside a function are not accessible (visible) from outside the function.
        Variables declared with var, let and const are quite similar when declared inside a function
*/

{
    let localVar = "Armando Perez";
}

let globalVar = "Mr. Worldwide";

// console.log(localVar);  - Error localvar is not defined
console.log(globalVar);    


function showNames(){
    //Function scoped variables
    var functionVar = "Arthas";
    const functionConst ="Porthas";
    let functionLet = "Aramis";

    console.log(functionVar);  
    console.log(functionConst);  
    console.log(functionLet);  
}

showNames();

// console.log(functionVar);  
// console.log(functionConst);  
// console.log(functionLet);  

//Nested Function

function myNewFunction(){
    let name = "Jane";

    function nestedFunction(){
        let nestedName = "John";
        console.log(name);       
    }
nestedFunction();
}
myNewFunction();
// nestedFunction(); different scope

// Function and Global Scoped Variables
    //Global Scoped Variable

    let globalName = "Bill Gates";

    function myNewFunction2(){
        let nameInside = "Steve Jobs";
        console.log(globalName) // from out to inside

        function myNewFunction3(){
            let nameInside = "Steve Jobs"; 
            console.log(nameInside) // from out to inside
        }
      myNewFunction3();
    }

    myNewFunction2();
    
    //console.log(nameInside); // function scoped cannnot be accessed globally

    //[SECTION] Alert
    //alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It allows us to show a short dialog or instruction to our user. The page will wait until the user dismisses the dialog.

    //alert("Hello");

    function showSampleAlert(){
        alert("hello user")
    }
    // showSampleAlert();

    console.log("I will only in the console when the alert is dismissed");

//PROMPT
// let samplePrompt = prompt("Enter your name");

// console.log("Hello " + samplePrompt);
// console.log(typeof samplePrompt);

// let sampleNullPrompt = prompt("Dont't enter anything");
// console.log(sampleNullPrompt);

//prompt() returns an empty string when there is no input. Or null if the user cancels the prompt().

/*

function printWelcomeMessage(){
    let firstName = prompt("Enter your first name");
    let lastName = prompt("Enter your last name");

    console.log("Hello " + firstName + " " + lastName + "!");
    console.log("Welcome to my page");    
};

printWelcomeMessage();

*/

//prompt() can be used to gather user input and be used in our code. However, since prompt() windows will have the page wait until the user dismisses the window it must not be overused.

//prompt() used globally will be run immediately, so, for better user experience, it is much better to use them accordingly or add them in a function.

//[SECTION] Function Naming Conventions
//Function names should be definitive of the task it will perform. It usually contains a verb.


//Avoid generic names to avoid confusion within your code.

function get(){

    let name = "Jamie";
    console.log(name);

};

get();

//Avoid pointless and inappropriate function names.

function foo(){

    console.log(25%5);

};

foo();

//Name your functions in small caps. Follow camelCase when naming variables and functions.

function displayCarInfo(){

    console.log("Brand: Toyota");
    console.log("Type: Sedan");
    console.log("Price: 1,500,000");

}

displayCarInfo();


//RETURN STATEMENT

function returnMe(){
    return true;
}

let boolean = returnMe();
console.log(boolean); //true

function returnThis(){
    console.log(false); 
}

let boolean2 = returnThis();

console.log(boolean2);



