
/*
	1. Create a function named getUserInfo which is able to return an object. 

		The object returned should have the following properties:
		
		- key - data type

		- name - String
		- age -  Number
		- address - String
		- isMarried - Boolean
		- petName - String

		Note: Property names given is required and should not be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.

*/

function getUserInfo (){
    let userInfo  = {
        name: "Tony",
        age: 30,
        address: "Avengers Tower",
        isMarried: false,
        petName: "Jarvis"
    };

    return userInfo;
};

let myUserInfo  = getUserInfo();
console.log(myUserInfo);

/*
	2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
		
		- Note: the array returned should have at least 5 elements as strings.
			    function name given is required and cannot be changed.


		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
	
*/

function getArtistsArray(){
    let artistArray = ["Thrice","Urbandub", "Franco", "Lamb of God", "Curbside"];

    return artistArray;
}
let myArtistArray = getArtistsArray();
console.log(myArtistArray);

/*
	3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/

function getSongsArray (){
    let songsArray = ["Snow","Slow dancing in a burning room", "Mr. Crowley", "Still got the blues", "November Rain"];

    return songsArray ;
}
let mySongsArray = getSongsArray();
console.log(mySongsArray);

/*
	4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/

function getMoviesArray(){
    let moviesArray = ["Minority Report", "Spirited Away", "Return of the King", "The Raid", "The Dark Knight"];

    return moviesArray ;
}
let myMoviesArray = getMoviesArray();
console.log(myMoviesArray);

/*
	5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

			- Note: the array returned should have numbers only.
			        function name given is required and cannot be changed.

			To check, create a variable to save the value returned by the function.
			Then log the variable in the console.

			Note: This is optional.
			
*/

function getPrimeNumberArray(){
    let primeNumberArray = [7, 11, 13, 17, 19];

    return primeNumberArray ;
}
let myprimeNumberArray = getPrimeNumberArray();
console.log(myprimeNumberArray );


/*---------------------------------------------------------------------------------*/
//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
		getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
		getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
		getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
		getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

	}
} catch(err){


}