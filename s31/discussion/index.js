//use the "require" directive to load Node.js
//A package or module is a software component that contains one or more routine/methods/function
//The http module lets nodejs transfer data  using http

let http = require("http");


//Using this module createServer() method, we can create an HTTP server that listens to a specied port

http.createServer(function (req, res){
    //writeHead() method
    //Set the status code for the response - 200 -> OK/SUCCESS
    res.writeHead(200, {"Content-type": "text/plain"});

    //Send the respose with text content to "Hello world"
    res.end("Hello world")
}).listen(4000);

//Whenever the server starts, console will print the message in terminal
console.log("Server running at localhost:4000");