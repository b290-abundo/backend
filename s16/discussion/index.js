console.log("Helllooooo");

///////////// Arithmetic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = x%y;
console.log("The remainder is " + remainder);

/////////////// Assignment Operators
// The assignment operator assigns the value of the **right** operand to a variable.

let assignmentNumber = 8;

// Addition assignment Operator (+=)
// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.

// assignmentNumber = assignmentNumber + 2;
// same output as simplified code below

console.log("AssignmentNumber is: ", assignmentNumber)

assignmentNumber += 2;
console.log("The result of the assignment operator (+= 2) is: " , assignmentNumber);

assignmentNumber -= 2;
console.log("The result of the assignment operator (-= 2) is: " , assignmentNumber);

assignmentNumber *= 2;
console.log("The result of the assignment operator (*= 2) is: " , assignmentNumber);

assignmentNumber /= 2;
console.log("The result of the assignment operator (/= 2) is: " , assignmentNumber);

assignmentNumber %= 2;
console.log("The result of the assignment operator (%= 2) is: " , assignmentNumber);


// Multiple Operators and Parentheses
/*
- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
            - The operations were done in the following order:
                1. 3 * 4 = 12
                2. 12 / 5 = 2.4
                3. 1 + 2 = 3
                4. 3 - 2.4 = 0.6
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas); 

let pemdas = 1 + (2-3) * (4/5);
console.log("Result of pemdas operation: " + pemdas); 
                
/*
            - By adding parentheses "()", the order of operations are changed prioritizing operations inside the parentheses first then following the MDAS rule for the remaining operations
            - The operations were done in the following order:
                1. 4 / 5 = 0.8
                2. 2 - 3 = -1
                3. -1 * 0.8 = -0.8
                4. 1 + -.08 = .2
        */

pemdas = (1 + (2-3)) * (4/5);
console.log("Result of pemdas operation: " , pemdas); 

//Increment Decrement

// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;
let increment = ++z;
console.log("Result of pre-increment: " + increment); 
console.log("Result of pre-increment: " + z); 

// The value of "z" is added by a value of one before returning the value and storing it in the variable "increment"

// The value of "z" was also increased even though we didn't implicitly specify any value reassignment

increment = z++;
console.log("Result of post-increment: " + increment); 
console.log("Result of post-increment: " + z); 

// The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one
// The value of "z" is at 2 before it was incremented

let decrement = --z;
console.log("Result of pre-decrement: " + decrement); 
console.log("Result of pre-decrement: " + z); 

// The value of "z" is decreased by a value of one before returning the value and storing it in the variable "decrement"

decrement = z--;
console.log("Result of post-decrement: " + decrement); 
console.log("Result of post-decrement: " + z); 

// The value of "z" was decreased reassigning the value to 1

//[SECTION] Comparison Operators

let juan = 'juan';

 //Equality operator

 /* 
- Checks whether the operands are equal/have the same content
- Attempts to CONVERT AND COMPARE operands of different data types
- Returns a boolean value
*/

 //(Loose) Equality operator
 console.log('/=============(Loose) Equality operator=================/');
 console.log(1==1);  //true
 console.log(1==2);  //false
 console.log(1=='1');  //true
 console.log(0== false);  //true
 console.log('juan'=='juan');  //true
 console.log('juan'==juan);  //true

 
 //(Strict) Equality operator

 /* 
- Checks whether the operands are equal/have the same content
- Also COMPARES the data types of 2 values
- JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
- In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
- Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
- Strict equality operators are better to use in most cases to ensure that data types provided are correct
*/

 //(Strict) Equality operator
console.log('/=============(Strict) Equality operator=================/');
console.log(1===1);  //true
console.log(1===2);  //false
console.log(1==='1');  //true
console.log(0=== false);  //true
console.log('juan'==='juan');  //true
console.log('juan'===juan);  //true

 //(Loose) Inequality operator
 console.log('/=============(Loose) Inequality operator =================/');
 console.log(1!=1);  
 console.log(1!=2);  
 console.log(1!='1');  
 console.log(0!=false);  
 console.log('juan'!='juan');  
 console.log('juan'!=juan);  

 //(Strict) Inequality operator
 /* 
- Checks whether the operands are not equal/have the same content
- Also COMPARES the data types of 2 values
*/
 //(Strict) Inequality operator
 console.log('/=============(Loose) Inequality operator =================/');
 console.log(1!==1);  
 console.log(1!==2);  
 console.log(1!=='1');  
 console.log(0 !==false);  
 console.log('juan'!=='juan');  
 console.log('juan'!== juan);  

 //[SECTION] Relational Operators
//Some comparison operators check whether one value is greater or less than to the other value.

let a = 50;
let b = 65;

//GT or Greater Than Operator (>)
let isGreaterThan = a > b;

//LT or Less Than Operator (>)
let isLessThan = a < b;

//GTE or Greater Than or Equal Operator (>=)
let isGTorEqual = a >= b;

//LTE or Less Than or Equal Operator (>=)
let isLTorEqual = a <= b;

console.log('/============= Relational Operators =================/');
console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

let numStr = '30';
console.log('/==============================/');
console.log(a > numStr); //true - converted to string
console.log(a <= numStr); //false

let str = "twenty";
console.log(b >= str); // false str resulted to NaN string is not numeric

//[SECTION] Logical Operators
let isLegalAge = true;
let isRegistered = false;

//AND Operator (&& or Double Ampersand)
let AllRequirementsMet = isLegalAge && isRegistered;
console.log('/============= Logical Operators =================/');
console.log("Result of logical AND Operator: " + AllRequirementsMet)

//OR Operator (|| or Double Pipe)
let SomeRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR Operator: " + SomeRequirementsMet)

/*
    AND same like multiplication
    10 = 0
    11 = 1
    01 = 0
    00 = 0

    OR Same like Addition
    10 = 1
    11 = 1
    01 = 1
    00 = 0
*/

//Not Operator -- opposite

let SomeRequirementsNotMet = !isRegistered;
console.log(SomeRequirementsNotMet);

