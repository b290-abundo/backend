//JavaScript renders web pages in an interactive and dynamic fashion. Meaning, it enables you to create dynamically updating content, control multimedia, animate images
    //Let get started by introducing the basic syntax elements of JavaScript.
    
    // [SECTION] Syntax, Statements and Comments

    //Statements:

    // Statements in programming are instructions that we tell the computer to perform
    // JS statements usually end with semicolon (;)
    // Semicolons are not required in JS, but we will use it to help us train to locate where a statement ends
    // A syntax in programming, it is the set of rules that describes how statements must be constructed
    // All lines/blocks of code should be written in a specific manner to work. This is due to how these codes were initially programmed to function and perform in a certain manner

    /*
		Where To Place JavaScript
			- Inline You can place JavaScript right into the HTML page using the script tags. This is good for very small sites and testing only. The inline approach does not scale well, leads to poor organization, and code duplication.
			-	External File A better approach is to place JavaScript into separate files and link to them from the HTML page. This way a single script can be included across thousands of HTML pages, and you only have one place to edit your JavaScript code. This approach is also much easier to maintain, write, and debug.
		
		Use of the Script Tag
			In the past, we had to worry about specifying many attributes for the script tag. 

		Where should I place the Script Tags?
			The script tags can go anywhere on the page, but as a best practice, many developers will place it just before the closing body tag on the HTML page. This provides faster speed load times for your web page.
*/

console.log("Hi!");

// [SECTION] Variables

		// It is used to contain data.
		// Any information that is used by an application is stored in what we call a "memory"
		// When we create variables, certain portions of a device's memory is given a "name" that we call "variables"
		// This makes it easier for us associate information stored in our devices to actual "names" about information


		// Declaring variables:

		// Declaring variables - tells our devices that a variable name is created and is ready to store data
		// Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was "not defined".

let firstVariable;

console.log(firstVariable);

// console.log() is useful for printing values of variables or certain results of code into the Google Chrome Browser's console
// Constant use of this throughout developing an application will save us time and builds good habit in always checking for the output of our code

// console.log(hello) - index.js:48 Uncaught ReferenceError: hello is not defined

// console.log(hello); - index.js:50 Uncaught ReferenceError: Cannot access 'hello' before initialization
// let hello  

// Variables must be declared first before they are used
// Using variables before they're declared will return an error

/*
    Guides in writing variables:
        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
        2. Variable names should start with a lowercase character, use camelCase for multiple words.
        3. For constant variables, use the 'const' keyword.
        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.

    Best practices in naming variables:

        1. When naming variables, it is important to create variables that are descriptive and indicative of the data it contains.

            let firstName = "Michael"; - good variable name
            let pokemon = 25000; - bad variable name

        2. When naming variables, it is better to start with a lowercase letter. We usually avoid creating variable names that starts with capital letters. Because there are several keywords in JS that start in capital letter.

            let FirstName = "Michael"; - bad variable name
            let firstName = "Michael"; - good variable name

        3. Do not add spaces to your variable names. Use camelCase for multiple words, or underscores.

            let first name = "Mike";

        camelCase is when we have first word in small caps and the next word added without space but is capitalized:

            lastName emailAddress mobileNumber

        Underscores sample:

        let product_description = "lorem ipsum"
        let product_id = "250000ea1000"
*/
let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);



// In the context of certain applications, some variables/information are constant and should not be changed
		// In this example, the interest rate for a loan, savings account or a mortgage must not be changed due to real world concerns
		// This is the best way to prevent applications from suddenly breaking or performing in ways that are not intended
// Reassigning variable values
		// Reassigning a variable means changing it's initial or previous value into another value

productName = "laptop";
console.log(productName);

/*
const interest = 3.599;
interest = 3.5;     ----- error reassigning const 
console.log(interest);   
*/

let friend = "Lilo";
console.log(friend);
friend = "Stitch";
console.log(friend);

// let friend = "Dory"; -- Uncaught SyntaxError: Identifier 'friend' has already been declared


/*
		    Guides in writing variables:
		        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
		        2. Variable names should start with a lowercase character, use camelCase for multiple words.
		        3. For constant variables, use the 'const' keyword.
		        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.

		    Best practices in naming variables:

				1. When naming variables, it is important to create variables that are descriptive and indicative of the data it contains.

					let firstName = "Michael"; - good variable name
					let pokemon = 25000; - bad variable name

				2. When naming variables, it is better to start with a lowercase letter. We usually avoid creating variable names that starts with capital letters. Because there are several keywords in JS that start in capital letter.

					let FirstName = "Michael"; - bad variable name
					let firstName = "Michael"; - good variable name

				3. Do not add spaces to your variable names. Use camelCase for multiple words, or underscores.

					let first name = "Mike";

				camelCase is when we have first word in small caps and the next word added without space but is capitalized:

					lastName emailAddress mobileNumber

				Underscores sample:

				let product_description = "lorem ipsum"
				let product_id = "250000ea1000"

		*/
    
    /*
		When to use JavaScript const?
			As a general rule, always declare a variable with const unless you know that the value will change.

			Use const when you declare:

				-A new Array
				-A new Object
				-A new Function

		Note: You can discuss later the constant array and objects once done with the topic of data types. It can only be an overview to explain that the const does not define a constant value. It defines a constant reference to a value so it can change the elements in an array and properties in an object.
		*/

let anime;  // declaration
anime = "Neon Genesis Evangelion"; //initialization
anime ="Gundam Seed"; //reassignment

// const pi; --- error const missing declaration
//pi = 3.1416;

//let/const local/global scope
		//Scope essentially means where these variables are available for use
		//let and const are block scoped
		//A block is a chunk of code bounded by {}. A block lives in curly braces. Anything within curly braces is a block.
		//So a variable declared in a block with let  is only available for use within that block

let outerVar = "Hello";
{
    let innerVar = "Hello again!"
}

console.log(outerVar)
// console.log(innerVar) -- error due to block scope



////////////////////////////////Global Scope

var a = 20;  //global, showing on windows and can be used in function below
let b = 40;  //not showing on windows but can be used in function below

function dcode(){
    return b;
}

//////////////////////////////Function Scope
//inaccessible from outside function
//only applies on the older var keyword

function test(){
    var x = 34;
}

//console.log(x); //x not accessible from outside the function
                //block scope also prevents let and const to be accessed outside

/////////////////////////////Block Scope
//inaccessible from outside the block
//only works on let and const 

if(100 > 20){
    let y = 20;
}
// console.log(y); //y inaccessible from outside the block
                //var y can be accessed outside block scope

//Note: 
//var can be redeclared but not let and const
//var uses function scope
//let and const uses block scope

///////////////////////Strings
let country = "Philippines"
let province = "Bulacan"

let fullAddress = province + ", " + country
console.log(fullAddress)

// Concatenating strings
		// Multiple string values can be combined to create a single string using the "+" symbol
// The escape character (\) in strings in combination with other characters can produce different effects

let mailAddress = "Bulacan, \n \n Philippines"; // new line
console.log(mailAddress)


let message = 'I\'m Groot'; // with escape character for '
console.log(message);

//Numbers
//Integers/Whole numbers
let headcount = 26;
console.log(headcount);

//decimal
let heatIndex = 40.1;
console.log(heatIndex);

//exponent
let planetDistance = 2e10;
console.log(planetDistance);

//string is the final type
console.log("The heat index today is", heatIndex);

// Boolean values are normally used to store values relating to the state of certain things
// This will be useful in further discussions about creating logic to make our application respond to certain scenarios

let isMarried = false;
let inGoodConduct = true;

console.log(isMarried);
console.log("isMarried:", isMarried);

//Array
// Arrays are a special kind of data type that's used to store multiple values
// Arrays can store different data types but is normally used to store similar data types


//list of same data
let grade = [98.7, 90, 91, 30];
console.log(grade);

//combination of data - recommended
let array1 = ['steve', 'tony', 'bruce', 32, true];
console.log(array1);

//Objects
// different data types
// storing different data types inside an array is not recommended because it will not make sense to in the context of programming
// Objects are another special kind of data type that's used to mimic real world objects/items
// They're used to create complex data that contains pieces of information that are relevant to each other
// Every individual piece of information is called a property of the object

let person = {
    fullName: "Juan Dela Cruz",
    age: 35,
    isMarried: false,
    contact: ["+639 123 4567 890", "7890 0123"],
    address: {
        houseNumber: '456',
        city: "Manila"
    }
}
console.log(person);

let myGrades = {
    firstGFrading: 98.7,
    secondGrading: 98.7,
    thirdGrading: 90.5,
}
console.log(myGrades);

//typeof
console.log(typeof myGrades);

/*
Constant Objects and Arrays
    The keyword const is a little misleading.

    It does not define a constant value. It defines a constant reference to a value.

    Because of this you can NOT:

    Reassign a constant value
    Reassign a constant array
    Reassign a constant object

    But you CAN:

    Change the elements of constant array
    Change the properties of constant object

*/

const animes = ["K-On", "Yakitate Japan", "Cyber Kuro-chan"];
animes[2] = ["Kimetsu no Yaiba"]; 

console.log(animes);

//NULL

// It is used to intentionally express the absence of a value in a variable declaration/initialization
// null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified

let spouse = null;
let myNumber = 0;
let myString = '';

//UNDEFINED

let feelings;
console.log(feelings);

//undefined vs null

// One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value
// null means that a variable was created and was assigned a value that does not hold any value/amount
// Certain processes in programming would often return a "null" value when certain tasks results to nothing

let varA = null;
console.log(varA);

let varB = null;
console.log(varB);


