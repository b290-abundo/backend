let http = require("http");
const port = 4000;

const app = http.createServer((req, res)=>{
    
    if(req.url == "/greeting"){
        res.writeHead(200, {"Content-type": "text/plain"});
        res.end("Hello Again!")

    }else if(req.url == "/homepage"){
        res.writeHead(200, {"Content-type": "text/plain"});
        res.end("This is the homepage") 

    }else{
        res.writeHead(200, {"Content-type": "text/plain"});
        res.end("page not found")  
    }
});

app.listen(port);
console.log(`Server running at localhost:${port}`);
