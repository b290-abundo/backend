//Array in programming is simply

let sudentNameA= "Steve";
let sudentNameB= "Tony";
let sudentNameC= "Bruce";
let sudentNameD= "Natasha";
let sudentNameE= "Thor";

// Arrays 

let studentNames = ["Ben", "Clyde", "Leri", "Mith", "Ralph"];

//[S E C T I O N]

/*
    - Arrays are used to store multiple related values in a single variable
    - They are declared using square brackets ([]) also known as "Array Literals"
    - Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks
    - Arrays also provide access to a number of functions/methods that help in achieving this
    - A method is another term for functions associated with an object and is used to execute statements that are relevant to a specific object
    - Majority of methods are used to manipulate information stored within the same object
    - Arrays are also objects which is another data type
    - The main difference of arrays with an object is that it contains information in a form of a "list" unlike objects which uses "properties"
    - Syntax
        let/const arrayName = [elementA, elementB, ElementC...]
*/
let grades = [98.5, 94.3, 89.3, 90.1];
let computeBrands = ["Acer", "Lenovo", "Asus"];

//Alternative ways to write an array:

 let myTasks =[
    "drink html",
    "eat javascript",
    "inhale css",
    "bake bootstrap"
 ];

 //Creating an array using variables:
 let city1 = "Tokyo";
 let city2 = "Manila";
 let city3 = "Nairobi";

 let cities = [city1, city2, city3];
 console.log(myTasks);
 console.log(cities);

 //[S E C T I O N] .length property
 //The .length property allows us to get and set the total number of items in an array. Spaces are counted as characters in strings.

 console.log(myTasks.length);
 console.log(cities.length);

 let blankArr = []; //truthy
 console.log(blankArr.length);

 //length property used with strings
let fullName = "Lucy Pevensie";
console.log(fullName.length);

//length property can also set the total number of items in an array, meaning we can actually delete the last item in the array or shorten the array by simply updating the length property of an array.

myTasks.length = myTasks.length - 1;
console.log(myTasks.length);
console.log(myTasks);

//using decrement for deleting the last element of an array
cities.length--;
console.log(cities);

//decrementaion will not work on strings
fullName.length = fullName.length -1;
console.log(fullName.length);
console.log(fullName);

let theBeatles = ["John", "Paul", "Ringo", "Geoge"];
theBeatles.length++;
console.log(theBeatles);

/*
you can also lengthen it by adding a number into the length property. Since we lengthen the array forcibly, there will be another item in the array, however, it will be empty or undefined. Like adding another seat but having no one to sit on it.
*/

 //[S E C T I O N] Reading from arrays
 /*
    - Accessing array elements is one of the more common tasks that we do with an array
    - This can be done through the use of array indexes
    - Each element in an array is associated with it's own index/number
    - In JavaScript, the first element is associated with the number 0 and increasing this number by 1 for every element
    - The reason an array starts with 0 is due to how the language is designed
    - Array indexes actually refer to an address/location in the device's memory and how the information is stored
    - Example array location in memory
        Array address: 0x7ffe9472bad0
        Array[0] = 0x7ffe9472bad0
        Array[1] = 0x7ffe9472bad4
        Array[2] = 0x7ffe9472bad8
    - In the example above, the first element and the array itself points to the same memory location and therefore is at 0 elements away from the location of the array itself
    - Syntax
        arrayName[index];
*/

console.log(grades[0]);
console.log(computeBrands[3]);

let LakersLegends = ["Kobe", "Shaq", "LeBron", "Magic", "Kareem"];
console.log(LakersLegends[1]);
console.log(LakersLegends[3]);

let player = LakersLegends[2];
console.log(player);

console.log("Array before reassignment");
console.log(LakersLegends);
LakersLegends[2] = "Davis";
console.log("Array before reassignment");
console.log(LakersLegends);

//Accesing the last element of the array

let bullsLegend = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
let lastElemIndex = bullsLegend.length - 1;
console.log(bullsLegend[lastElemIndex]);
console.log(bullsLegend[bullsLegend.length-1]);

//Add items in the array
//Using indices, you can also add items into the array.

const newArr = [];
console.log(newArr[0]); //undefined since no lements yet

newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[1] = "Tifa Lockhart";
console.log(newArr);

newArr[newArr.length] = "Zack Fair";
console.log(newArr);

//Looping over array
//You can use a for loop to iterate over all items in an array.
//Set the counter as the index and set a condition that as long as the current index iterated is less than the length of the array, we will run the condition. It is set this way because the index of an array starts at 0.

for (let index = 0; index < newArr.length; index++) {
    console.log(newArr[index]);
}

let numArr = [5, 12, 30, 46, 40];
    for (let index = 0; index < numArr.length; index++) {

    if (numArr[index] % 5 ===0 ) { ;
        console.log(numArr[index] + " is divisible by 5");
    }else {
        console.log(numArr[index] + " is NOT divisible by 5");
    } 

}

 //[S E C T I O N] Multi dimenstional arrays

 /*
    - Multidimensional arrays are useful for storing complex data structures
    - A practical application of this is to help visualize/create real world objects
    - Though useful in a number of cases, creating complex array structures is not always recommended.
*/

let chessboard = [
['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'],
];

console.log(chessboard);

//Accessing
console.log(chessboard[1][4]);


