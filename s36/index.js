//Dependencies
const express = require('express');
const mongoose = require('mongoose');

const taskRoute = require("./routes/taskRoute.js")

//Server
const app = express();
const port= 4000;

//Database
mongoose.connect('mongodb+srv://admin:admin123@zuitt.3falo1m.mongodb.net/b290-to-do?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

//Database connection
let db = mongoose.connection
db.on('error', console.error.bind(console, "connection error"));
db.once("open", () => console.log("MongoDB Atlas connected."));


//Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Setup routes
app.use('/tasks', taskRoute);


if(require.main === module){
    app.listen(port, () => {
            console.log(`Server running on port ${port}`);
        }); 
}

module.exports = app;
