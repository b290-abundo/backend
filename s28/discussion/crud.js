//CRUD Operations
// [SECTION] Inserting Documents (Create)

//Inserting a single document
//syntax: db.collectionName.insertOne({object})
db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact:{
        phone: "09123456789",
        email: "janedoe@gmail.com"
    },
    courses: ["CSS", "Javascript", "Python"],
    department: "none"
});


//Inserting multiple document
//syntax: db.collectionName.insertmany([{object}, {object}])

db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact:{
            phone: "09123456789",
            email: "stephehawking@gmail.com"
        },
        courses: ["Python", "React", "PHP"],
        department: "none"
    }, 
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact:{
            phone: "09123456789",
            email: "neilarmstrong@gmail.com"
        },
        courses: ["React", "Laravel", "Sass"],
        department: "none"
    },
])

//SECTION Fiding documets (Read)

db.users.find();

db.users.find({firstName: "Stephen"});

db.users.find({lastName: "Armstrong", age: 82});



db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact:{
        phone: "00000000",
        email: "test@gmail.com"
    },
    courses: [],
    department: "none"
})

//update test plus additional porperties
db.users.updateOne({firstName: "Test"},
{
    $set: {
        firstName: "Bill",
        lastName: "Gates",
        age: 65,
        contact:{
            phone: "09123456789",
            email: "bill@gmail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations",
        status: "active" 
    }
})

db.users.find({firstName: "Bill"});

//update phone only
db.users.updateOne(
    {firstName : "Test"},
    {
        $set : {
            "contact.phone" : "9999999999999"
        }
    }
)

//delete user
db.users.deleteMany({ firstName: "Bill" });

//update many
db.users.updateMany({department: "none"},
    {
        $set:
        {department: "HR"}
    }
)

//replace one
db.users.replaceOne({ firstName: "Bill" },
    {
        firstName: "Bill",
        lastName: "Gates",
        age: 65,
        contact:{
            phone: "09123456789",
            email: "bill@gmail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations"
    } 
)

//replace entire entry
db.users.replaceOne({ firstName: "Bill" },
    {
        firstName: "Bill",
        lastName: "Gates"
    } 
)


////////////////////////////////delete 


//document to be deleted

db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
})

//delete one  document
db.users.deleteOne({ firstName: "Test" });

//delete many  document
db.users.deleteMany({ firstName: "Bill" });


//SECTION for advanced queries
db.users.find({
    contact:{
        phone: "09123456789",
        email: "stephehawking@gmail.com"
    }
})

db.users.find({"contact.email": "janedoe@gmail.com"})

//querying and array with exact contents
db.users.find({courses: ["CSS", "Javascript", "Python"]})

//Querying an array without regard to order
db.users.find({courses: {$all: ["React", "Python"]}})

db.users.insertOne({
    namearr: [
        {
            namea: "juan"
        },
        {
            nameb: "tamad"
        }

    ]
})


db.users.find({
    namearr:{
        namea: "juan"
    }
})
