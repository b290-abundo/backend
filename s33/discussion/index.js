// [Section] Javascript Synchronous vs Asynchronous

// Javascript is by default is synchronous meaning that only one statement is executed at a time

////////////////////////////////Synchronous 
/*
    console.log("Hello world");
    //cosole.log("Again"); //will cause error
    console.log("Bye"); //this will not be run, (blocked)


    for (let i = 0; i <=1500; i++) {
        console.log(i);  
    }
    console.log("Henlo"); //it will take long to be logged
*/
//////////////////////////////// Asynchronous 

// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background

// The Fetch API allows you to asynchronously request for a resource (data)

// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value

/*
    Syntax:
        fetch("URL")
*/

console.log(fetch("https://jsonplaceholder.typicode.com/posts")) //Promise

// Retrieves all posts following the Rest API (retrieve, /posts, GET)
// By using the then method we can now check for the status of the promise
fetch("https://jsonplaceholder.typicode.com/posts")
// The "fetch" method will return a "promise" that resolves to a "Response" object
// The "then" method captures the "Reponse" object and returns another "promise" which will eventually be "resolved" or "rejected"
    .then((response) => console.log(response.status));

fetch("https://jsonplaceholder.typicode.com/posts")
    // Use the "json" method from the "Response" object to convert the data retrieved into JSON format to be used in our application
   .then((response) => response.json())
   // Print the converted JSON value from the "fetch" request
	// Using multiple "then" methods creates a "promise chain"
   .then((json) => console.log(json));  

   // The "async" and "await" keywords is another approach that can be used to achieve asynchronous code


// Used in functions to indicate which portions of code should be waited for

//Creates and asynchronous function 
async function fetchData(){
    // waits for the "fetch" method to complete then stores the value in the "result" variable
    let result = await fetch("https://jsonplaceholder.typicode.com/posts");

    //Result returned by the fetch returns a promise
    console.log(typeof result);
    console.log(result);
    console.log(result.body);

    let json = await result.json();

    console.log(json);
}
fetchData();


//[SECTION] Getting a specific post

// Retrieve a specific post folowing REST API 
//(/posts/:id/, GET)
fetch("https://jsonplaceholder.typicode.com/posts/7")
    .then((response) => response.json())
    .then(json => console.log(json));


//[SECTION] Adding a post

/*
    Syntax:
        fetch("URL", options)
        .then(res =>{})
        .then(res =>{})
*/

//Create a new post following the REST API
fetch("https://jsonplaceholder.typicode.com/posts",
    {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(
        {
            title: "New Post",
            body: "This is a new post",
            id: "1",
        })
    }
).then(response => response.json())
    .then(json => console.log(json));


//[SECTION] Updating a post
// /posts/:id
fetch("https://jsonplaceholder.typicode.com/posts/1",{

    method: "PUT",
    headers: {"Content-Type": "application/json"},
    body: JSON.stringify({
        id: "1",
        title: "Updated Post",
        body: "This is an updated post",
        userId: "1"
    })  
}).then(response => response.json())
.then(json => console.log(json));

//MINI ACTIVITY
//[SECTION]  Updating using PATCH Method
fetch("https://jsonplaceholder.typicode.com/posts/3",{

    method: "PATCH",
    headers: {"Content-Type": "application/json"},
    body: JSON.stringify({
        title: "Updated Post"
    })  
}).then(response => response.json())
.then(json => console.log(json));

//[SECTION]  Deleting  Post
fetch("https://jsonplaceholder.typicode.com/posts/4",{
    method: "DELETE"
    });


//[SECTION]  Filtering Posts
// The data can be filtered by sending the userId along with the URL
// Information sent vie the url can be done by adding the question mark symbol (?)

/*
    Syntax:
        Individual Parameter
        "url?parameterName=value"
        Multiple Parameters
        "url?parameterName1=valueA&parameterNameB=valueB"   
*/
fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
    .then(response => response.json())
    .then(json => console.log(json));

//[SECTION] Retrieving nested/related commennts to posts
fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
    .then(response => response.json())
    .then(json => console.log(json));
