//SECTION ObJects

/*
/*
- An object is a data type that is used to represent real world objects
- It is a collection of related data and/or functionalities
- In JavaScript, most core JavaScript features like strings and arrays are objects (Strings are a collection of characters and arrays are a collection of data)
- Information stored in objects are represented in a "key:value" pair
- A "key" is also mostly referred to as a "property" of an object
- Different data types may be stored in an object's property creating complex data structures
*/

// Creating objects using object initializers/literal notation
/*
- This creates/declares an object and also initializes/assigns it's properties upon creation
- A cellphone is an example of a real world object
- It has it's own properties such as name, color, weight, unit model and a lot of other things
- Syntax
    let objectName = {
        keyA: valueA,
        keyB: valueB
    }
*/
let cellphone = {
    name: "Nokia 3310",
    manufactureDate: 1999,
}

console.log("Result of creating objects using initializers/literal notation: ");
console.log(cellphone);
console.log(typeof cellphone);

// Creating Objects using constructor function
    /*
- Creates a reusable function to create several objects that have the same data structure
- This is useful for creating multiple instances/copies of an object
- An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
- Syntax
    function ObjectName(keyA, keyB) {
        this.keyA = keyA;
        this.keyB = keyB;
    }
*/


/*
This is an object
The "this" keyword allows to assign a new object's properties by associating them with values received from a constructor function's parameters
*/    
function Laptop(name, manufactureDate){
    this.name = name;
    this.manufactureDate = manufactureDate;
}

let laptop = new Laptop("Lenovo", 2008); 
console.log("Result of creating objects using object constructors: ");
console.log(laptop);

// This is a unique instance of the Laptop object
/*
    - The "new" operator creates an instance of an object
    - Objects and instances are often interchanged because object literals (let object = {}) and instances (let object = new object) are distinct/unique objects
*/

let myLaptop = new Laptop("MacBook Air", 2020); 
console.log("Result of creating objects using object constructors: ");
console.log(myLaptop);


// This is a unique instance of the Laptop object
/*
    - It invokes/calls the "Laptop" function instead of creating a new object instance
    - Returns "undefined" without the "new" operator because the "Laptop" function does not have a return statement
*/
//Creating empty objects
let computer = {};
let MyComputer = new Object(); 
console.log(computer);
console.log(MyComputer);

///////////////////////Accessing Object properties
//Using the dot notation
console.log("Result of the dot notation: " + myLaptop.name);

//Using the  square brackets notation
console.log("Result of the dot notation: " + myLaptop["name"]);
 
//Accessing array objects
/*
- Accessing array elements can be also be done using square brackets
- Accessing object properties using the square bracket notation and array indexes can cause confusion
- By using the dot notation, this easily helps us differentiate accessing elements from arrays and properties from objects
- Object properties have names that makes it easier to associate pieces of information
*/
let array = [laptop, myLaptop];
console.log(array[0]["name"]);
console.log(array[0].name);

// May cause confusion for accessing array indexes
// Differentiation between accessing arrays and object properties
// This tells us that array[0] is an object by using the dot notation

////////////////////Initializing/adding/Deleting/Reassigning
let car = {};
car.name = "Honda Civic";
console.log("Result of adding properties using dot notation: ");
console.log(car);

// Initializing/adding object properties using bracket notation
/*
    - While using the square bracket will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accesssed using the square bracket notation
    - This also makes names of object properties to not follow commonly used naming conventions for them
*/
car["manuacture date"] = 2019; 
console.log(car["manuacture date"]);
console.log(car["Manuacture Date"]); // undefined
console.log(car["manuactureDate"]); // undefined
console.log("Result of adding properties using square bracket: ");
console.log(car);

// Delete object properties
delete car["manufacture date"];
console.log("Result of deleting properties: ");
console.log(car);

//reassigning object properties
car.name = "Volswagen Beetle";
console.log("Result of resassining properties: ");
console.log(car);


/*
	MINI ACTIVITY 

	Create your own person object using constructor and 2 instances:
	name, age, personality, device unit
*/
function Person(name, age, personality, deviceUnit) {
    this.name = name;
    this.age = age;
    this.personality = personality;
    this.deviceUnit = deviceUnit;
  }

  var Thor = new Person("Thor", 30, "Hostile", "Hammer");
  var Steve = new Person("Steve", 31, "Friendly", "Shield");

  console.log(Steve);
  console.log(Thor);

  //[SECTION] Object Methods
/*
    - A method is a function which is a property of an object
    - They are also functions and one of the key differences they have is that methods are functions related to a specific object
    - Methods are useful for creating object specific functions which are used to perform tasks on them
    - Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work
*/    

let person = {
    name: "Juan",
    talk: function(){
        console.log("Kumusta? Ako si " + this.name);
    }
}
console.log(person);
console.log("Result for object methods: ");
person.talk();

person.walk = function() {
    console.log(this.name +  " walked 25 steps forward");   
};
person.walk();

let friend = {
    firstName: "Pedro",
    lastName: "Penduko",
    address: {
        city: "manila",
        country: "Philippines"
    },
    emails: ["pedro@gmail.com", "penduko@gmail.com"],
    intoduce: function(){
        console.log("Hello! My name is " + this.firstName + " " + this.lastName);
    }
};
friend.intoduce();
console.log(friend);



// [ S E C T I O N ] Real-World Application of Objects
/*
    - Scenario
        1. We would like to create a game that would have several pokemon interact with each other
        2. Every pokemon would have the same set of stats, properties and functions
*/
// Using object literals to create multiple kinds of pokemon would be time consuming
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
	},
	faint: function(){
		console.log("Pokemon fainted")
	}
};

console.log(myPokemon);

// Creating an object constructor instead will help with this process
function Pokemon(name, level){

	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to _targetPokemonhealth_");
	};
	this.faint = function(){
		console.log(this.name + " fainted.");
	}
};

// Creates new instances of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
pikachu.tackle(rattata);

    
    
