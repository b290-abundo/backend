const bcrypt = require('bcrypt');
const auth = require('../auth');
const Course = require('../models/Course');
const User = require('../models/User');

module.exports.checkEmailExists = (reqBody) => {
    return User.find({email: reqBody.email}).then(result => {
        if (result.length > 0) {
            return true;
        } else {
            return false;
        }
    })
}

// User registration
		/*
			Steps:
			1. Create a new User object using the mongoose model and the information from the request body
			2. Make sure that the password is encrypted
			3. Save the new User to the database
		*/
// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
			// Uses the information from the request body to provide all the necessary information

module.exports.registerUser = (reqBody) =>{

    let newUser = new User({
         firstName: reqBody.firstName,
         lastName: reqBody.lastName,
         email: reqBody.email,
         mobileNo: reqBody.mobileNo,
         password: bcrypt.hashSync(reqBody.password, 10)
    })
//Save the created object to the database
    return newUser.save()
    .then(user => true)
    .catch(err => {
        console.log(err);

        return false;
    });
}

/*
    Steps:
    1. Check the database if the user email exists
    2. Compare the password provided in the login form with the password stored in the database
    3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody.email})
  .then(result => {
    //if user does not exist
    if (result == null) {
        return false;

    //user exists    
    }else {
        const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

        if (isPasswordCorrect) {
            return {access: auth.createAccessToken(result)}
        }else {
            return false; //password did not match
        }
    }
  })   
}

module.exports.getProfile = (data) => {

    return User.findById(data.userId).then((result) => {

        if (!result) {
            return false;
        } else {
            result.password = "";
            return result;
        }
      })
      .catch((err) => {
        console.log(err);
        return false;
      });
  };
  

  // Enroll user to a class
    /*
        Steps:
        1. Find the document in the database using the user's ID
        2. Add the course ID to the user's enrollment array
        3. Update the document in the MongoDB Atlas Database
    */
    // Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user

module.exports.enroll = async (data) => {

    // Add the course ID in the enrollments array of the user
    // Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
	// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend
    // Adds the courseId in the user's enrollments array

    let isUserUpdated = await User.findById(data.userId).then(user => {

        user.enrollments.push({courseId: data.courseId})

        // Saves the updated user information in the database
        return user.save().then(user => true).catch(err => {

            console.log(err)
            return false;
        })

    }).catch(err => {

        console.log(err)
        console.log("error in retrieving")
        return false;
    });


    let isCourseUpdated = await Course.findById(data.courseId).then(course =>{
        
        course.enrollees.push({userId : data.userId})

        return course.save().then(course => true).catch(err => {

            console.log(err)
            return false;
        });

    }).catch(err => {

        console.log(err)
        console.log("error in retrieving")
        return false;
    });


    // Condition that will check if the user and course documents have been updated
	// User enrollment successful
    return (isUserUpdated && isCourseUpdated)? true : false;

}