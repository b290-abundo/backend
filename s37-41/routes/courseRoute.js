const express = require("express");
const router = express.Router();

const auth = require('../auth'); //

const courseController = require("../controllers/courseController");

// Rote for creating a course
router.post("/", auth.verify, (req, res) => {

	// Solution #1

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		console.log({ auth : "unauthorized user"})
		res.send(false);
	}


	// Solution #2
/*	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	courseController.addCourse(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
*/
});


router.get("/all", (req, res) => {
    courseController
    .getAllCourses(req.body)
    .then((resultFromController) => res.send(resultFromController));
});


router.get("/all", auth.verify, (req, res) => {

    const userData= auth.decode(req.headers.authorization)

    if (userData.isAdmin == false) {
        res.send("User is not admin!")
    } else {
        courseController
        .getAllCourses(req.body)
        .then((resultFromController) => res.send(resultFromController));
    }
    
});
// Route for retrieving all the ACTIVE courses
// Middleware for verifying JWT is not required because users who aren't logged in should also be able to view the courses
router.get("/", (req, res) => {
    courseController.getAllActiveCourses()
    .then((resultFromController) => res.send(resultFromController));
})

// Route for retrieving a specific course
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url

router.get("/:courseId", (req, res) =>{
    console.log(req.params)

    courseController
    .getCourse(req.params)
    .then((resultFromController) => res.send(resultFromController));
})

router.put("/:courseId", auth.verify, (req, res) =>{

    const isAdmin = auth.decode(req.headers.authorization).isAdmin  


    if (isAdmin) {
        courseController
        .updateCourse(req.params, req.body)
        .then((resultFromController) => res.send(resultFromController));
        
    } else {
        console.log({auth: "unauthorized"})
        res.send("false")
    }   
})

//ACtivity s40

router.patch("/:courseId/archive", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if (userData && userData.isAdmin) {
      console.log(req.params);

      courseController
        .archiveCourse(req.params, req.body)
        .then((resultFromController) => res.send(resultFromController));

    } else {
        res.send("Not authorized");
    }
  });
  


module.exports = router;
