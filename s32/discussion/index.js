// Use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol
// The "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Clients (browser) and servers (node JS/express JS applications) communicate by exchanging individual messages.
// The messages sent by the client, usually a Web browser, are called requests
// The messages sent by the server as an answer are called responses.
let http = require("http");
const port = 4000;

const app = http.createServer((req, res) => {
// The HTTP method of the incoming request can be accessed via the "method" property of the "request" parameter
// The method "GET" means that we will be retrieving or reading information
    if(req.url == "/items" && req.method == "GET"){
        // Requests the "/items" path and "GETS" information
        res.writeHead(200, {"Content-type": "text/plain"});
        // Ends the response process
        res.end("Data retrieved from the database.");
    };
        // The method "POST" means that we will be adding or creating information
    if(req.url == "/items" && req.method =="POST"){
        //Requests the items path and SENDs information
        res.writeHead(200, {"Content-type": "text/plain"});
        res.end("Data to be sent to the database.");
    }
});

app.listen(port, () => console.log(`Server running at localhost:${port}`));
