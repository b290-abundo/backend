const auth = require('../auth');
const Course = require("../models/Course");
const User = require("../models/User.js");


// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

// Solution #1
module.exports.addCourse = reqBody => {
	// MINI ACTIVITY
	// Create/Instantiate a nerw object for new Course
	// Stretch goals: return the saved new Course

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newCourse = new Course({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});

	// Saves the created object to our database
	return newCourse.save().then(course => true)
		.catch(error => {

			console.log(error);

			return false
		})
};

// Solution #2
/*module.exports.addCourse = data => {
	if (data.isAdmin) {

		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		return newCourse.save().then(course => true)
			.catch(error => {

				console.log(error);

				return false
		});
	} else {

		console.log({ auth : "unauthorized user"})

		return false
	}

};*/

/*MINI ACTIVITY*/
/*
	Steps:
	1. Retrieve all the courses from the database
*/
// Retrieve all courses



module.exports.getAllCourses = () => {

    return Course.find({}).then(result => result).catch(err => {
        console.log(err)
        return false
    });
}

// Retrieve all ACTIVE courses
    /*
        Steps:
        1. Retrieve all the courses from the database with the property of "isActive" to true
    */

module.exports.getAllActiveCourses = () => {

    return Course.find({isActive : true}).then(result => result).catch(err => {
        console.log(err)
        return false
    });
}


module.exports.getCourse = (reqParams) => {

    return Course.findById(reqParams.courseId)
    .then(result => result).catch(err => {
        console.log(err)
        return false
    });

};


// Update a course
		/*
			Steps:
			1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
			2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
		*/
		// Information to update a course will be coming from both the URL parameters and the request body


module.exports.updateCourse = (reqParams, reqBody) => {

    // Specify the fields/properties of the document to be updated
    let updatedCourse = {
        name : reqBody.name,
        description : reqBody.description,
        price : reqBody.price
    }

    // Syntax
	// findByIdAndUpdate(document ID, updatesToBeApplied)
    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
    .then((course) => true).catch((error) => {
        console.log(error)
        return false
    })

}

//ACtivity s40

module.exports.archiveCourse = (reqParams, reqBody) => {

    return Course.findByIdAndUpdate(reqParams.courseId, reqBody)
    .then(result => true)
        .catch((err) => {
            console.log(err);
            return false;
        });

};
  