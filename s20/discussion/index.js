//[SECTION] while loop
/*
    - A while loop takes in an expression/condition
    - Expressions are any unit of code that can be evaluated to a value
    - If the condition evaluates to true, the statements inside the code block will be executed
    - A statement is a command that the programmer gives to the computer
    - A loop will iterate a certain number of times until an expression/condition is met
    - "Iteration" is the term given to the repetition of statements
    - Syntax
        while(expression/condition) {
            statement
        }
*/

/* ////////////////////////////////////////////////////////////////////////////

        let count = 5;

        while (count !== 0) {
            console.log("while: " + count);

            count --;
        }

*/ ////////////////////////////////////////////////////////////////////////////
      
    // Decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
    // Loops occupy a significant amount of memory space in our devices
    // Make sure that expressions/conditions in loops have their corresponding increment/decrement operators to stop the loop
    // Forgetting to include this in loops will make our applications run an infinite loop which will eventually crash our devices
    // After running the script, if a slow response from the browser is experienced or an infinite loop is seen in the console quickly close the application/browser/tab to avoid this
    //- must meet the condition before execution of the loop

//[SECTION] Do-while loop

/*
    - A do-while loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.
    - Syntax
        do {
            statement
        } while (expression/condition)
*/

/* ////////////////////////////////////////////////////////////////////////////

        let number = Number(prompt("Give me a number: "));

        do { 

            console.log("Do While: " + number);
            number += 1;

        } while(number < 10);

*/ ////////////////////////////////////////////////////////////////////////////


//[SECTION] For loop

/*
    - A for loop is more flexible than while and do-while loops. It consists of three parts:
        1. The "initialization" value that will track the progression of the loop.
        2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
        3. The "finalExpression" indicates how to advance the loop.
    - Syntax
        for (initialization; expression/condition; finalExpression) {
            statement
        }
*/

/*  ////////////////////////////////////////////////////////////////////////////
        for (let count = 0; count <=20; count++) {
            console.log(count);
        }
*/  ////////////////////////////////////////////////////////////////////////////

// Characters in strings may be counted using the .length property
// Strings are special compared to other data types in that it has access to functions and other pieces of information another primitive data type might not have
// Accessing elements of a string
// Individual characters of a string may be accessed using it's index number
// The first character in a string corresponds to the number 0, the next is 1 up to the nth number

/*  ////////////////////////////////////////////////////////////////////////////

            myString = "Francis"

            for (let x = 0; x < myString.length; x++) {

                console.log(myString[x]);
                
            }

*/////////////////////////////////////////////////////////////////////////////

/*  ////////////////////////////////////////////////////////////////////////////
myString = "FrAncIs"

        for (let i = 0; i < myString.length; i++) {

            console.log(myString[i].toLowerCase() );
            
        }
*/////////////////////////////////////////////////////////////////////////////
   
// Mini - Activity
    // Create and if-else statement that will check lowercased vowels
    // if it is a vowel print "*"
    // else print the letter of the name
    


//[SECTION] For loop - Continue and Break
/*
    - The "continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
    - The "break" statement is used to terminate the current loop once a match has been found
*/

/*  ////////////////////////////////////////////////////////////////////////////

for (let count = 0; count <= 20; count++) {
    // if remainder is equal to 0
    if (count % 2 === 0) {
        console.log(count);
        continue;   
    }   

    // Tells the code to continue to the next iteration of the loop
    // This ignores all statements located after the continue statement;
    console.log("Continue and Break: " + count);

    if(count > 10){
        break;
    }
}
*/////////////////////////////////////////////////////////////////////////////


let word = "pneumonoultramicroscopicsilicovolcanoconiosis";

for(let index = 0; index < word.length; index++){
    console.log(word[index]);
    
    if(word[index].toLocaleLowerCase() === 'a'){
        console.log("Continue to the nex iteration: ");
        continue;
    }
    if (word[index]==='v') {
        break;
    }
}
