// Controllers contain the functions and business logic of our Express JS application
// Meaning all the operations it can do will be placed in this file

// Uses the "require" directive to allow access to the "Task" model which allows us to access Mongoose methods to perform CRUD functions
// Allows us to use the contents of the "Task.js" file in the "models" folder
const Task = require("../models/Task.js");

// Controller function for getting all the tasks
// Defines the functions to be used in the "taskRoute.js" file and exports these functions
module.exports.getAllTasks = () => {

	// The "return" statement, returns the result of the Mongoose method "find" back to the "taskRoute.js" file which invokes this function when the "/tasks" routes is accessed
	// The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/Postman
	return Task.find({}).then(result => {

		// The "return" statement returns the result of the MongoDB query to the "result" parameter defined in the "then" method
		return result;
	})
};

// Controller function for creating a task
// The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
module.exports.createTask = (requestBody) => {

	console.log(requestBody);

	// Creates a task object based on the Mongoose model "Task"
	let newTask = new Task({
		// Sets the "name" property with the value received from the client/Postman
		name : requestBody.name
	});

	return newTask.save().then((task, error) => {

		if(error){

			console.log(error);
			return false

		} else {

			return task
		}
	});
};

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then(removedTask => {

		return removedTask;

	}).catch(err => {

		console.log(err)
		return false
	})
};

// Controller function for updating a task

// Business Logic
/*
	1. Get the task with the id using the Mongoose method "findById"
	2. Replace the task's status returned from the database with the "status" property from the request body
	3. Save the task
*/

// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
// The updates to be applied to the document retrieved from the "req.body" property coming from the client is renamed as "newContent"
module.exports.updateTask = (taskId, newContent) => {

	console.log(newContent)

	return Task.findById(taskId).then(result => {

		result.name = newContent.name;

		return result.save().then((updatedTask, saveErr) => {

			if(saveErr){

				console.log(saveErr);
				return false
			} else {

				return updatedTask;
			}
		})
	}).catch(err => console.log(err));
}

// [Section] Activity

// Controller function for getting a specific task

// Business Logic
/*
	1. Get the task with the id using the Mongoose method "findById"
*/

// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
module.exports.getTask = (taskId) => {

	// The "findById" Mongoose method will look for a task with the same id provided from the URL
	return Task.findById(taskId).then((result, error) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(error){

			console.log(error);
			return false;

		// Find successful, returns the task object back to the client/Postman
		} else {

			return result;	
		}
	})

}

// Controller function for updating a task status to "complete"

// Business Logic
/*
	1. Get the task with the id using the Mongoose method "findById"
	2. Change the status of the document to complete
	3. Save the task
*/

// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
module.exports.completeTask = (taskId) => {

	// The "findById" Mongoose method will look for a task with the same id provided from the URL
	// The "return" statement, returns the result of the Mongoose method "findById" back to the "taskRoute.js" file which invokes this function 
	return Task.findById(taskId).then((result, err) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(err){

			console.log(err);
			return err;

		}

		// Change the status of the returned document to "complete"
		result.status = "complete";

		// Saves the updated object in the MongoDB database
		// The document already exists in the database and was stored in the "result" parameter, we can use the "save" method to update the existing document with the changes we applied
		// The "return" statement returns the result of the "save" method to the "then" method chained to the "findById" method  which invokes this function
		return result.save().then((updatedTask, saveErr) => {

			// If an error is encountered returns a "false" boolean back to the client/Postman
			if (saveErr) {

				console.log(saveErr);
				// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
				return saveErr;

			// Update successful, returns the updated task object back to the client/Postman
			} else {

				// The "return" statement returns the result to the  "then" method chained to the "save" method which invokes this function
				return updatedTask;

			}
		})
	})

}